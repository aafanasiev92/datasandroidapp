package com.aafanasiev.datasandroidapp;

/**
 * Created by Aleksandr on 05.09.2017.
 */

public class Tips {
    private String tipsTitle;
    private String tipsText;

    public Tips(String tipsTitle, String tipsText) {
        this.tipsTitle = tipsTitle;
        this.tipsText = tipsText;
    }

    public String getTipsTitle() {
        return tipsTitle;
    }

    public void setTipsTitle(String tipsTitle) {
        this.tipsTitle = tipsTitle;
    }

    public String getTipsText() {
        return tipsText;
    }

    public void setTipsText(String tipsText) {
        this.tipsText = tipsText;
    }
}
