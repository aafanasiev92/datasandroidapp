package com.aafanasiev.datasandroidapp;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PreloadActivity extends AppCompatActivity {

    private Button continueBtn;
    private RecyclerView tipsRecyclerView;
    private HorizontalAdater horizontalAdapter;
    private List<Tips> tipsList;
    private ConstraintLayout preloadView;
    private ConstraintLayout loadingView;
    private ConstraintLayout loginView;
    private ProgressBar progressBar;
    private EditText phone;
    private TextView loginPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preload);

        setupView();

        tipsList = fillTipsList();

        horizontalAdapter = new HorizontalAdater(tipsList, getApplicationContext());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(PreloadActivity.this, LinearLayoutManager.HORIZONTAL, false);
        tipsRecyclerView.setLayoutManager(horizontalLayoutManager);
        tipsRecyclerView.setAdapter(horizontalAdapter);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAG", "onClick: DONE");
                preloadView.setVisibility(View.INVISIBLE);
                loadingView.setVisibility(View.VISIBLE);
                stopAnimation(4);
            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            String phoneStr = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(phoneStr)) {

                    if (s.length() == 1 && !s.toString().equals("+")) {
                        Log.d("TAG", "1: " + s);
                        phoneStr = "+" + s;
                    } else if (s.length() == 3 && !s.toString().equals("(")) {
                        phoneStr = s + "(";
                    } else if (s.length() == 7 && !s.toString().equals(")")) {
                        phoneStr = s + ")";
                    } else {
                        Log.d("TAG", "2: " + s);
                        if (count > before) {
                            phoneStr += s.toString().toCharArray()[s.length() - 1];
                        } else {
                            phoneStr = s.toString();
                        }
                    }
                    phone.setText(phoneStr);
                    phone.setSelection(phone.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        loginPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void stopAnimation(int seconds) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setIndeterminate(false);
                loadingView.setVisibility(View.INVISIBLE);
                loginView.setVisibility(View.VISIBLE);
            }
        }, seconds*1000);

    }

    private List<Tips> fillTipsList() {

        List<Tips> tipses = new ArrayList<>();
        tipses.add(new Tips("Текст типсы", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"));
        tipses.add(new Tips("Текст типсы 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"));
        tipses.add(new Tips("Текст типсы 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"));
        return  tipses;
    }

    private void setupView() {
        continueBtn = (Button)findViewById(R.id.continue_btn);
        tipsRecyclerView = (RecyclerView) findViewById(R.id.tipsRecyclerView);
        preloadView = (ConstraintLayout)findViewById(R.id.preloadView);
        loadingView = (ConstraintLayout)findViewById(R.id.loadingView);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        loginView = (ConstraintLayout)findViewById(R.id.loginView);
        phone = (EditText)findViewById(R.id.phone_editText);
        loginPassword = (TextView)findViewById(R.id.login_password);
    }


    public class HorizontalAdater extends RecyclerView.Adapter<HorizontalAdater.MyViewHolder> {

        private List<Tips> tipsList = new ArrayList<>();
        private Context context;

        public HorizontalAdater(List<Tips> tipsList, Context context) {
            this.tipsList = tipsList;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tips_row, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            holder.tipsTitle.setText(tipsList.get(position).getTipsTitle());
            holder.tipsText.setText(tipsList.get(position).getTipsText());
        }

        @Override
        public int getItemCount() {
            return tipsList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView tipsTitle;
            private TextView tipsText;

            public MyViewHolder(View itemView) {
                super(itemView);
                tipsTitle = (TextView)itemView.findViewById(R.id.tips_title);
                tipsText = (TextView)itemView.findViewById(R.id.tips_text);
            }
        }
    }
}